@rem (c) https://github.com/MontiCore/monticore  
pushd %~dp0
cd ../..
set AUTOPILOT_HOME=%CD%
popd
set SIMULATION_URL=http://localhost:8080/
set JAVA_HOME=%AUTOPILOT_HOME%\jdk
set TOMCAT_HOME=%AUTOPILOT_HOME%\apache-tomcat-9.0.5
set MINGW_HOME=%AUTOPILOT_HOME%\mingw64
set ARMADILLO_HOME=%AUTOPILOT_HOME%\armadillo-8.400.0
set AUTOPILOT_CPP_DIR=%AUTOPILOT_HOME%\cpp
set AUTOPILOT_DLL_DIR=%AUTOPILOT_HOME%\dll
set PATH=%AUTOPILOT_DLL_DIR%;%JAVA_HOME%\bin;%MINGW_HOME%\bin;%ARMADILLO_HOME%\examples\lib_win64;%PATH%
