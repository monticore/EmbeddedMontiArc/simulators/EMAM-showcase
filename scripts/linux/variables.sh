cd ../..
# (c) https://github.com/MontiCore/monticore  
AUTOPILOT_HOME="$(pwd)"

mkdir -p $AUTOPILOT_HOME/cpp
mkdir -p $AUTOPILOT_HOME/dll

# Replace it with your machine's java home path
JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home

SIMULATION_URL=http://localhost:8080/
TOMCAT_HOME=$AUTOPILOT_HOME/apache-tomcat-9.0.5
MINGW_HOME=$AUTOPILOT_HOME/mingw64
ARMADILLO_HOME=$AUTOPILOT_HOME/armadillo-8.400.0
AUTOPILOT_CPP_DIR=$AUTOPILOT_HOME/cpp
AUTOPILOT_DLL_DIR=$AUTOPILOT_HOME/dll
