#!/bin/bash
# (c) https://github.com/MontiCore/monticore  

source ./variables.sh

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	OS="linux";
elif [[ "$OSTYPE" == "darwin" ]]; then
	OS="darwin";
fi

g++ -shared -fPIC \
    -I $JAVA_HOME/include/ \
    -I $JAVA_HOME/include/$OS \
    -I $ARMADILLO_HOME/include \
    -o $AUTOPILOT_DLL_DIR/AutopilotAdapter.dll \
    $AUTOPILOT_CPP_DIR/AutopilotAdapter.cpp \
