#!/bin/bash
# (c) https://github.com/MontiCore/monticore  

source "./variables.sh"

java -jar $AUTOPILOT_HOME/emam2cpp.jar \
    --output-dir=$AUTOPILOT_CPP_DIR \
    --models-dir=$AUTOPILOT_HOME/model \
    --root-model=de.rwth.armin.modeling.autopilot.autopilot \
    --flag-use-armadillo-backend \
    --flag-generate-autopilot-adapter
