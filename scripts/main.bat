@rem (c) https://github.com/MontiCore/monticore  
call variables.bat
call generate.bat
call compile.bat
call start-simulation.bat
timeout /nobreak 20
start %SIMULATION_URL%
pause
call stop-simulation.bat
