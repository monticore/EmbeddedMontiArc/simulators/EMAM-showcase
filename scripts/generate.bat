@rem (c) https://github.com/MontiCore/monticore  
if exist "%AUTOPILOT_CPP_DIR%" rmdir "%AUTOPILOT_CPP_DIR%" /s /q
mkdir "%AUTOPILOT_CPP_DIR%"
"%JAVA_HOME%\bin\java.exe" -jar "%AUTOPILOT_HOME%\emam2cpp.jar" ^
   --models-dir="%AUTOPILOT_HOME%\model" ^
   --root-model=de.rwth.armin.modeling.autopilot.autopilot ^
   --output-dir="%AUTOPILOT_CPP_DIR%" ^
   --flag-use-armadillo-backend ^
   --flag-generate-autopilot-adapter
