@rem (c) https://github.com/MontiCore/monticore  
call variables.bat
call stop-simulation.bat
timeout /nobreak 3
call start-simulation.bat
timeout /nobreak 20
start %SIMULATION_URL%
