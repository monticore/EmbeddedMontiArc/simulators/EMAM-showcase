@rem (c) https://github.com/MontiCore/monticore  
if exist "%AUTOPILOT_DLL_DIR%" rmdir "%AUTOPILOT_DLL_DIR%" /s /q
mkdir "%AUTOPILOT_DLL_DIR%"
g++ -shared -fPIC ^
   -I"%JAVA_HOME%\include" ^
   -I"%JAVA_HOME%\include\win32" ^
   -I"%ARMADILLO_HOME%\include" ^
   -L"%ARMADILLO_HOME%\examples\lib_win64" ^
   -o "%AUTOPILOT_DLL_DIR%\AutopilotAdapter.dll" ^
   "%AUTOPILOT_CPP_DIR%\AutopilotAdapter.cpp" ^
   -lblas_win64_MT -llapack_win64_MT
