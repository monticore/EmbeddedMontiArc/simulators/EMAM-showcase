<!-- (c) https://github.com/MontiCore/monticore -->
# EMAM Autopilot model showcase

## Requirements:
 * `Windows 64 bit`
 * `ROOt.war`

## Installation
   * clone the repository using
   
          cd MontiSim
          git clone https://github.com/MontiSim/EMAM-showcase.git
          
      or download the project as a [.zip file](https://github.com/MontiSim/EMAM-showcase/archive/master.zip).
   * Download [ROOT.war](https://github.com/MontiSim/EMAM-showcase/releases/download/webapp/ROOT.war) and place it at `/apache-tomcat-9.0.5/webapps/`.
   
__NOTE:__ _MontiSim_ is the directory, in which MontiSim-belonging projects should be clonned. It has to be manually created.
    
## Usage:

 Go to `scripts` folder
 
* For Windows users:
    * run `main.bat` __from there__ i.e. the script assumes that the working directory is `scripts`
* For Mac/Linux users:
    * edit the `JAVA_HOME` in `linux/variables.sh` to fit your machine.
    * run `linux/generate.sh` and then `linux/compile.sh` __from there__.

Available one can find a [video example](https://github.com/MontiSim/EMAM-showcase/releases/download/webapp/example1.mp4) of using the model.

# Known issues

## Simulation never ending

The AutoPilot is desined to calculate simulation frames until a timeout expires, causing the simulator to stop.

In environment of shared simulator, which serves many simulations, such timeout cannot be used, thus either a _simulationDone_ flag has to be provided by the model or _lastNavigationTarget_ node has to be available.

In the distributed simulator, which is such an environment, the AutoPilot behaviour results into a non-stopping  simulation, thus the simulation buffer used to store simulation frames is constantly kept full, until a user stops the scenario simulation.


